var express = require("express");
var app = express();
const chalk = require('chalk');
const bodyParser = require("body-parser");
const fs = require("fs");
var dataFile = "./json/data.json";
var data = require(dataFile);


app.use(express.static("./public"));
app.use(bodyParser.json());

app.post("/api", (req, res) => {
    console.log(req.body);
    acceleration(req.body);
    res.sendStatus(200);
})

app.get("/api/data", (req, res) => {
    res.send(data);
})

app.listen(3000, () => {
    console.log(chalk.blue("Server initilized"));
})

function acceleration(json) {
    var array = Object.values(json.data);
    var total = 0;
    array.map(element => {
        if (element > total || (element < 0 && element < total)) {
            total = element;
        }
    })
    var intentos = data.data.datasets[0].intentos + 1;
    data.data.datasets[0].data[intentos] = total;
    data.data.labels[intentos] = total;
    data.data.datasets[0].intentos = intentos;
    fs.writeFile(dataFile, JSON.stringify(data), function(err) {
        if (err) {
            console.log(err);
            return
        }
        console.log(JSON.stringify(data));
        console.log("writing data to: ", dataFile);
    });
}