window.onload = function() {
    getData().then((response) => {
        return response.json()
    }).then((data) => {
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, data);
    })

}

function getData() {
    return fetch("/api/data")
}